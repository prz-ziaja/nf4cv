# NF4CV

Normalizing flows for computer vision

## Goal and resuts
The main aim of the project was to test if preprocessing with normalizing flows can cause faster training time or better results.  
Processing time and obtained results suggest that it is not worth to apply such a transformation. 
Results are better but not significantly better.